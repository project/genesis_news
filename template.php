<?php
// $Id: template.php,v 1.10 2011/01/14 02:57:57 jmburnz Exp $

/**
 * Preprocess and Process Functions SEE: http://drupal.org/node/254940#variables-processor
 * 1. Rename each function to match your subthemes name,
 *    e.g. if you name your theme "themeName" then the function
 *    name will be "themeName_preprocess_hook". Tip - you can
 *    search/replace on "genesis_kathy".
 * 2. Uncomment the required function to use.
 */

/**
 * Override or insert variables into all templates.
 */
/* -- Delete this line if you want to use these functions
function genesis_kathy_preprocess(&$vars, $hook) {
}

function genesis_kathy_process(&$vars, $hook) {
}
// */

/**
 * Override or insert variables into the html templates.
 */
/* -- Delete this line if you want to use these functions
function genesis_kathy_preprocess_html(&$vars) {
  // Uncomment the folowing line to add a conditional stylesheet for IE 7 or less.
  // drupal_add_css(path_to_theme() . '/css/ie/ie-lte-7.css', array('weight' => CSS_THEME, 'browsers' => array('IE' => 'lte IE 7', '!IE' => FALSE), 'preprocess' => FALSE));
}
function genesis_kathy_process_html(&$vars) {
}
// */

/**
 * Override or insert variables into the page templates.
 */
/* -- Delete this line if you want to use these functions
function genesis_kathy_preprocess_page(&$vars) {
}
function genesis_kathy_process_page(&$vars) {
}
// */

/**
 * Override or insert variables into the node templates.
 */
/* -- Delete this line if you want to use these functions
function genesis_kathy_preprocess_node(&$vars) {
}
function genesis_kathy_process_node(&$vars) {
}
// */

/**
 * Override or insert variables into the comment templates.
 */
/* -- Delete this line if you want to use these functions
function genesis_kathy_preprocess_comment(&$vars) {
}
function genesis_kathy_process_comment(&$vars) {
}
// */

/**
 * Override or insert variables into the block templates.
 */
/* -- Delete this line if you want to use these functions
function genesis_kathy_preprocess_block(&$vars) {
}
function genesis_kathy_process_block(&$vars) {
}
// */

//----------------------------
//file template.php
//----------------------------
/*function genesis_kathy_preprocess_node(&$vars) {
    //comments
if($vars['node']->links['comment_comments']){
           $vars['link_comment'] =  l($vars['node']->links['comment_comments']['title'], $vars['node']->links['comment_comments']['href'],
               array(
                 'attributes' => array('class' => 'comment', 'title' => $vars['node']->links['comment_comments']['attributes']['title']),
                   'fragment' => $vars['node']->links['comment_comments']['fragment']
               )
     );         
   }

//comment_add
  if($vars['node']->links['comment_add']){
        $vars['link_comment_add'] =  l($vars['node']->links['comment_add']['title'], $vars['node']->links['comment_add']['href'],
     array(
             'attributes' => array('class' => 'comment-add', 'title' => $vars['node']->links['comment_add']['attributes']['title']),
               'fragment' => 'comment-form'
            )
      );         
   }

//attachments
  if($vars['node']->links['upload_attachments']){
     $vars['link_attachments'] =  l($vars['node']->links['upload_attachments']['title'], $vars['node']->links['upload_attachments']['href'], array('attributes' => array('class' => 'attachments','title' => $vars['node']->links['upload_attachments']['attributes']['title'] )));         
   }

//read more
    if($vars['node']->links['node_read_more']){
     $vars['link_read_more'] =  l($vars['node']->links['node_read_more']['title'], $vars['node']->links['node_read_more']['href'], array('attributes' => array('class' => 'read-more','title' => $vars['node']->links['node_read_more']['attributes']['title'] )));         
   }

//statistics_counter
   if($vars['node']->links['statistics_counter']){
     $vars['statistics_counter'] = $vars['node']->links['statistics_counter']['title'];
  }
}
*/
 
/*
function genesis_kathy_preprocess(&$vars, $hook) {
	
//unset($vars['content']['links']['node']['#links']['node-readmore']);	
	
    // In some themes it can be '$variables' in the others - '$vars'.
  $node = $vars['node'];

  // Check first the "body field" exists or not.
  $field = field_get_items('node', $node, 'body');

  // If available do execution ..
  if($field) {
    $show_read_more = 1;
    if(array_key_exists($GLOBALS['language_content']->language, $node->body))
    {
        $body = $node->body[$GLOBALS['language_content']->language][0]['safe_value'];
        if(stristr($body, "<!--break-->")) // Lets make sure that this is indeed the end of article.
        {
            $rest_of_the_text = substr($body, strpos($body, "<!--break-->"));
            if(strlen($rest_of_the_text)<strlen("<!--break--></p><p>&nbsp;</p>"))
                $show_read_more = 0;
        }
        else if ($vars['content']['body'][0]['#markup'] == $body)
            $show_read_more = 0;
    }
    if($show_read_more == 0)
        unset($vars['content']['links']['node']['#links']['node-readmore']);
  } 
 
}
*/

/* Don't know how to get this to work - Add image as breadcrumb separator

function genesis_news_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];

  if (!empty($breadcrumb)) {
    // Provide a navigational heading to give context for breadcrumb links to
    // screen-reader users. Make the heading invisible with .element-invisible.
    $output = '<h2 class="element-invisible">' . t('You are here') . '</h2>';
    $breadcrumb_separator = '<span class="breadcrumb-separator">&nbsp;&nbsp;&nbsp;&nbsp;</span>';
    $output .= '<div class="breadcrumb">' . implode($breadcrumb_separator, $breadcrumb) . '</div>';
    return $output;
  }
}

*/